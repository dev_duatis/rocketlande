﻿using System;
using RocketLander.Contracts;

namespace RocketLander
{
    public class RocketLander
    {
        private IRocket lastLanded;
        private readonly Func<IRocket, bool> isInsideBoundsDelegate;

        public RocketLander(ILandingPlatform landingPlatform)
        {
            isInsideBoundsDelegate = (rocket) =>
            LadingPositionHelper
            .IsInsideBounds(
                landingPlatform.PosX,
                landingPlatform.PosY,
                landingPlatform.Size,
                rocket.LandingX,
                rocket.LandingY);
        }

        public LandingOutcome Query(IRocket rocket)
        {
            if(isInsideBoundsDelegate(rocket))
            {
                if(rocketClash(rocket)) return LandingOutcome.Clash;
                
                lastLanded = rocket;

                return LandingOutcome.Landed;
            }

            return LandingOutcome.OutOfBounds;
        }

        private bool rocketClash(IRocket rocket)
        {
            return lastLanded != null && LadingPositionHelper.IsInsideBounds(lastLanded.LandingX - 1, lastLanded.LandingY - 1 , 3, rocket.LandingY, rocket.LandingY); 
        }
    }
}
