﻿using System;
namespace RocketLander.Contracts
{
    public interface IRocket
    {
        public int LandingX { get; set; }
        public int LandingY { get; set; }
    }
}
