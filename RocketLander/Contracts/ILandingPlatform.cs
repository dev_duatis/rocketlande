﻿using System;
namespace RocketLander.Contracts
{
    public interface ILandingPlatform
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public int Size { get; set; }
    }
}
