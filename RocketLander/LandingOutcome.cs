﻿using System;
namespace RocketLander
{
    public enum LandingOutcome
    {
        Landed,
        OutOfBounds,
        Clash
    }
}
