﻿using System;
namespace RocketLander
{
    public class LadingPositionHelper
    {
        /// <summary>
        /// Checks if a point given by: posX, posY
        /// lays inside a sqare located at leftPos, topPos whith side length: side
        /// </summary>
        /// <param name="leftPos">left corner x position of the square</param>
        /// <param name="topPos">top corner y position of the square</param>
        /// <param name="side">size of the sqare´s side</param>
        /// <param name="posX">x coordinate of the point</param>
        /// <param name="posY">y coordinate of the point</param>
        /// <returns></returns>
        public static bool IsInsideBounds(int leftPos, int topPos, int side, int posX, int posY)
        {
            // transform positions relative to the square
            var absPosX = posX - leftPos;
            var absPosY = posY - topPos;

            return absPosX >= 0 && absPosY >= 0 && absPosX < side && absPosY < side;
        }
    }
}
