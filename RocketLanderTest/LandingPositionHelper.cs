﻿using RocketLander;
using Xunit;

namespace RocketLanderTest
{
    public class LandingPositionHelperTest
    {
        [Theory]
        [InlineData(15, 15, 10, 15, 15, true)]
        [InlineData(5, 5, 10, 15, 15, false)]
        [InlineData(5, 5, 10, 5, 5, true)]
        [InlineData(5, 5, 10, 16, 16, false)]
        [InlineData(0, 0, 10, 6, 6, true)]
        public void testLandingPositionHelper(int left, int top, int side, int x, int y, bool expectedOutcome)
        {
            var outcome = LadingPositionHelper.IsInsideBounds(left, top, side, x, y);
            Assert.Equal(expectedOutcome, outcome);
        }
    }
}
