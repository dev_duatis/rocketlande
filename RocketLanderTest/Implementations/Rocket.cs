﻿using System;
using RocketLander.Contracts;

namespace RocketLanderTest.Implementations
{
    public class Rocket : IRocket
    {
        public int LandingX { get; set; }
        public int LandingY { get; set; }
        public Rocket(int x, int y)
        {
            LandingX = x;
            LandingY = y;
        }
    }
}
