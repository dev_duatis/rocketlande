﻿using System;
using RocketLander.Contracts;

namespace RocketLanderTest.Implementations
{
    public class LandingPlatform: ILandingPlatform
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public int Size { get; set; }

        public LandingPlatform(int posX, int posY, int size)
        {
            PosX = posX;
            PosY = posY;
            Size = size;
        }
    }
}
