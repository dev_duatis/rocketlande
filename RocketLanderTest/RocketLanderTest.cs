﻿using RocketLanderTest.Implementations;
using RocketLander;
using Xunit;
namespace RocketLanderTest
{
    public class RocketLanderTest
    {
        [Theory]
        [InlineData(15, 15, 10, 15, 15, LandingOutcome.Landed)]
        [InlineData(5, 5, 10, 15, 15, LandingOutcome.OutOfBounds)]
        [InlineData(5, 5, 10, 5, 5, LandingOutcome.Landed)]
        [InlineData(5, 5, 10, 16, 16, LandingOutcome.OutOfBounds)]
        [InlineData(0, 0, 10, 6, 6, LandingOutcome.Landed)]
        public  void testRocketLander(int left, int top, int side, int x, int y, LandingOutcome  expectedOutcome)
        {
            var landingPlatform = new LandingPlatform(left, top, side);
            var rocket = new Rocket(x, y);

            var outcome = new RocketLander.RocketLander(landingPlatform).Query(rocket);

            Assert.Equal(expectedOutcome, outcome);
            
        }

        [Fact]
        public void testRocketClashSamePosition()
        {
            var landingPlatform = new LandingPlatform(15, 15, 10);
            var rocket = new Rocket(18, 18);
            var lander = new RocketLander.RocketLander(landingPlatform);

            lander.Query(rocket);
            var outcome = lander.Query(rocket);

            Assert.Equal(LandingOutcome.Clash, outcome);
        }

        [Theory]
        [InlineData(18,18,19,18, LandingOutcome.Clash)]
        [InlineData(18,18,18,19, LandingOutcome.Clash)]
        [InlineData(18,18,16,16, LandingOutcome.Landed)]
        [InlineData(18,18,5,5, LandingOutcome.OutOfBounds)]
        public void testRocketClashDifferentPosition(int x1, int y1, int x2,int y2, LandingOutcome expectedOutcome)
        {
            var landingPlatform = new LandingPlatform(15, 15, 10);
            var rocket = new Rocket(x1, y1);
            var rocket2 = new Rocket(x2, y2);
            var lander = new RocketLander.RocketLander(landingPlatform);

            lander.Query(rocket);
            var outcome = lander.Query(rocket2);

            Assert.Equal(expectedOutcome, outcome);
        }
    }
}
